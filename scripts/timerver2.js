let intervalId;
let sttimems = new Date().getTime();
let staButton= document.getElementById('stabutton');
let stoButton= document.getElementById('stobutton');
let resButton= document.getElementById('resbutton');
let endButton= document.getElementById('finbutton');
let curr = function timer(time){
    let seconds = Math.floor((time / 1000) % 60);
    let minutes = Math.floor((time / (1000 * 60)) % 60);
    let hours = Math.floor(((time / (1000 * 60 * 60)) % 24) + 7);
    hours = hours < 10 ? '0' + hours : hours;
    hours = hours > 23 ? hours - 24 : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return hours + " : " + minutes + " : " + seconds;
}
let currd = function dtimer(time1, time2){
    let diff = time1 - time2;
    let seconds = Math.floor((diff / 1000) % 60);
    let minutes = Math.floor(((diff / 1000) / 60) % 60);
    let hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
    let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    days = days < 10 ? '0' + days : days;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return days + " : " + hours + " : " + minutes + " : " + seconds;
}

if (localStorage.getItem("status") !== "on") {
    staButton.addEventListener('click',
        function () {
            document.getElementById('sttime').innerHTML = curr(sttimems);
            localStorage.setItem("staTime", String(sttimems));
            localStorage.setItem("status", "on");
            localStorage.setItem("nstatus", "wait");
        });
    staButton.addEventListener('click',
        function () {
            intervalId = setInterval(function () {
                let currentime = new Date().getTime();
                document.getElementById('rutime').innerHTML = currd(currentime, sttimems);
            }, 1000);
        });
} else if (localStorage.getItem("status") === "on"){
    let stat = Number(localStorage.getItem("staTime"));
    document.getElementById('sttime').innerHTML = curr(stat);
    let check = localStorage.getItem("nstatus")
    switch (check){
        case "wait" :
            intervalId = setInterval(
                function () {
                    let currenttime = new Date().getTime();
                    document.getElementById('rutime').innerHTML = currd(currenttime,stat);
                }, 1000);
            stoButton.addEventListener('click',
                function(){
                    let currnettime = new Date().getTime();
                    localStorage.setItem("nstatus","stop");
                    localStorage.setItem("stoTime",String(currnettime));
                    localStorage.setItem("diffTime",String(currnettime - stat));
                    clearInterval(intervalId);
                });
            break;
        case "stop":
            clearInterval(intervalId);
            if (localStorage.getItem("staTimeNew")) {
                let stavalue = Number(localStorage.getItem("staTimeNew"));
                let stovalue = Number(localStorage.getItem("stoTime"));
                let difftime = Number(localStorage.getItem("diffTime"));
                localStorage.setItem("difftime",String(stovalue + difftime - stavalue));
                document.getElementById('rutime').innerHTML = currd((stovalue + difftime), stavalue);
            } else if (!localStorage.getItem("staTimeNew") && localStorage.getItem("stoTime")){
                let stavalue = Number(localStorage.getItem("staTime"));
                let stovalue = Number(localStorage.getItem("stoTime"));
                localStorage.setItem("difftime",String(stovalue - stavalue));
                document.getElementById('rutime').innerHTML = currd(stovalue, stavalue);
            }
            resButton.addEventListener('click',
                function(){
                    let restime = new Date().getTime();
                    let difftime = Number(localStorage.getItem("diffTime"))
                    localStorage.setItem("staTimeNew",String(restime));
                    intervalId = setInterval(
                        function (){
                            let currenttime = new Date().getTime();
                            document.getElementById('rutime').innerHTML = currd((currenttime + difftime), restime);
                        },1000)
                    localStorage.setItem("nstatus","run");
                })
            break;
        case "run":
            let stavalue = Number(localStorage.getItem("staTimeNew"));
            let difftime = Number(localStorage.getItem("diffTime"));
            intervalId = setInterval(
                function(){
                    let currenttime = new Date().getTime();
                    document.getElementById('rutime').innerHTML = currd((currenttime + difftime), stavalue);
                },1000);
            stoButton.addEventListener('click',
                function() {
                    clearInterval(intervalId);
                    let stavalue = Number(localStorage.getItem("staTimeNew"));
                    let stovalue = Number(localStorage.getItem("stoTime"));
                    let difftime = Number(localStorage.getItem("diffTime"));
                    document.getElementById('rutime').innerHTML = currd((stovalue + difftime), stavalue);
                    localStorage.setItem("diffTime",String(difftime + stovalue - stavalue));
                    localStorage.setItem("nstatus","stop");
                })
            break;
    }
}
endButton.addEventListener('click',
    function () {
        let currentime = new Date().getTime();
        clearInterval(intervalId);
        document.getElementById('entime').innerHTML = curr(currentime);
    })
endButton.addEventListener('click',
    function () {
        let currentime = new Date().getTime();
        let stavalue = Number(localStorage.getItem("staTimeNew"));
        let diffvalue = Number(localStorage.getItem("diffTime"));
        document.getElementById('rutime').innerHTML = currd((currentime + diffvalue),stavalue);
    })
endButton.addEventListener('click',
    function () {
        localStorage.clear();
    });