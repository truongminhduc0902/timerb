let intervalId;
let sttimems = new Date().getTime()
let staButton = document.getElementById('stbutton');
let endButton = document.getElementById('fibutton');
let curr = function timer(time){
    let seconds = Math.floor((time / 1000) % 60);
    let minutes = Math.floor((time / (1000 * 60)) % 60);
    let hours = Math.floor(((time / (1000 * 60 * 60)) % 24) + 7);
    hours = hours < 10 ? '0' + hours : hours;
    hours = hours > 23 ? hours - 24 : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return hours + " : " + minutes + " : " + seconds;
}
let currd = function dtimer(time1, time2){
    let diff = time1 - time2;
    let seconds = Math.floor((diff / 1000) % 60);
    let minutes = Math.floor(((diff / 1000) / 60) % 60);
    let hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
    let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    days = days < 10 ? '0' + days : days;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return days + " : " + hours + " : " + minutes + " : " + seconds;
}

if (localStorage.getItem("status") !== "on" || localStorage === null ){
    staButton.addEventListener('click',
        function () {
            document.getElementById('sttime').innerHTML = curr(sttimems);
            localStorage.setItem("staTime", String(sttimems));
            localStorage.setItem("status", "on");
        })
    staButton.addEventListener('click',
        function () {
            intervalId = setInterval(function () {
                let currentime = new Date().getTime();
                document.getElementById('rutime').innerHTML = currd(currentime, sttimems);
                localStorage.setItem("runTime", String(currentime));
            }, 1000)
        })
    endButton.addEventListener('click',
        function () {
            let currentime = new Date().getTime();
            document.getElementById('entime').innerHTML = curr(currentime);
        })
    endButton.addEventListener('click',
        function () {
            let currentime = new Date().getTime();
            document.getElementById('rutime').innerHTML = currd(currentime, sttimems);
            clearInterval(intervalId);
        })
    endButton.addEventListener('click',
        function (){
            localStorage.clear();
        })
}else {
    let stavalue = Number(localStorage.getItem("staTime"));
    document.getElementById('sttime').innerHTML = curr(stavalue);
    intervalId = setInterval(function (){
        let stavalue = Number(localStorage.getItem("staTime"));
        let runvalue = new Date().getTime();
        document.getElementById('rutime').innerHTML = currd(runvalue,stavalue);
    },1000);
    endButton.addEventListener('click',
        function () {
            let currentime = new Date().getTime();
            document.getElementById('entime').innerHTML = curr(currentime);
        })
    endButton.addEventListener('click',
        function () {
            let currentime = new Date().getTime();
            let stavalue = Number(localStorage.getItem("staTime"));
            document.getElementById('rutime').innerHTML = currd(currentime,stavalue);
            clearInterval(intervalId);
        })
    endButton.addEventListener('click',
        function () {
            localStorage.clear();
        })
    endButton.addEventListener('click',
        function (){

    })
}